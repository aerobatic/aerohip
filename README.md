# Aerobatic to HipChat

A front-end web app to demonstrate posting a message to a HipChat channel via a
web form. Utilizes the [Aerobatic](https://www.aerobatic.com) HTTP Proxy plugin.

## Install

`$ cp .env.example .env`

then edit .env to include your webhook URL from
 https://YOUR_SUBDOMAIN.hipchat.com/services